CTYPE html>
<head>
<meta charset="utf-8"/>
<title>Coffee Results</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
<h1>Coffee Ratings</h1>

<?php

	if (!(isset($_POST['coffee']))){
		echo "error, no coffee input, exiting";
		return;
	}

$coffee = (string) $_POST['coffee'];


printf("Name: %s <br>Average rating (out of 5): %.1f <br><br>", $coffee, $rating);


#connect to database
$mysqli = new mysqli('localhost', 'jclukas', 'USDkwo101', 'coffee');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

$sql = "select id from coffees where name=".$coffee;
$stmt = $mysql->prepare($sql);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$result = $stmt->get_result();
$row = $result->fetch_assoc();
$coffeeId = (int) htmlspecialchars($row['name']);

$sql = "select rating from ratings where coffeeId=".$coffeeId;

$stmt = $mysql->prepare($sql);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$result = $stmt->get_result();

$ratings_count = 0;
$ratings_total = 0;
while ($row = $result->fetch_assoc()){
	$ratings_count = $ratings_count +1;
	$ratings_total = $ratings_total + htmlentities($row['rating']);
}

$average = ((float) $ratings_total)/((float) $ratings_count);


printf("Name: %s <br>Average rating (out of 5): %.1f <br><br>", $coffee, $average);

$sql = "select * from ratings where coffeeId=".$coffeeId;

$stmt = $mysql->prepare($sql);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$result = $stmt->get_result();

while($row = $result->fetch_assoc()){
	printf("%s : %d		%s <br>",$row['rating'],$row['comment']);
}


?>


<form action="coffee-main.html" method="POST">
	<p><input type="submit" value="Submit a New Rating" name="submit"></p>
</form>
 
</div></body>
</html>

