create database coffee;

create table coffees ( id int unsigned auto_increment, name varchar(50), primary key(id) ) engine = INNODB default character set = utf8 collate = utf8_general_ci;

create table ratings( coffeeId int unsigned not null, user varchar(50), rating decimal(1,1), comment tinytext, foreign key (coffeeId) references coffees (id)) engine = innodb default character set = utf8 collate = utf8_general_ci;
