<!DOCTYPE html>
<html>
<head>
	<title>HomePage</title>
	<link rel="stylesheet" type="text/css" href="layout.css" />
</head>
<body>

<?php
	if (isset($_POST['user']) && isset($_POST['coffee']) && isset($_POST['userRating']) && isset($_POST['comments'])){
		echo "all inputs initialized<br>";
	} else {
		echo "not all inputs were initialized, rating not recorded<br>";
		return;
	}

$user = (string) $_POST['user'];
$coffee = (string) $_POST['coffee'];
$userRating = (int) $_POST['userRating'];
$comments = (string) $_POST['comments'];


#connect to database
$mysqli = new mysqli('localhost', 'jclukas', 'USDkwo101', 'coffee');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

$sql = "insert into coffees (name) values (?)";
$stmt = $mysqli->prepare($sql);

if(!$stmt){
	printf("Insert Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('s',$coffee);

$stmt->execute();


$sql = "select id from coffees where name=".htmlentities($coffee);
$stmt = $mysqli->prepare($sql);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$result = $stmt->get_result();
$row = $result->fetch_assoc();
$coffeeId = htmlspecialchars($row['name']);


$sql = "insert into ratings (coffeeId, user, rating, comment) values (?,?,?,?)";

$stmt = $mysqli->prepare($sql);
$stmt->bind_param('isss',$coffeeId,$user,$rating,$comment);

#check success of insert
if(!$stmt){
	printf("Insert Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();

header("Location: coffee-main.html");


?>




</body>
</html>
